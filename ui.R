## Program: ui.R
## Author: Yuetong Liu
## Date: July, 2019

## Purpose:
## This program is the user interface of the sampling shiny app.

## Open Package ---------------------------------------------------
library(htmltools)
library(shinyBS)
library(rstudioapi)
library(yaml)
library(readxl)
library(plotly)
library(dplyr)


## Load all files in UploadFile and SystemFileFolder -----------------------------------

systemFileList <- list.files(IEPath)
systemFileList <- systemFileList[substring(systemFileList, nchar(systemFileList) - 8, nchar(systemFileList)) == ".sas7bdat"]

## Max upload size
options(shiny.maxRequestSize = 30 * 1024^2)

## UI
shinyUI(
  dashboardPage(
    ## Part 1: Header -------------------------
    dashboardHeader(title = "PPD Sampling Dashboard", titleWidth = 300),
    
    ## Part 2: Side --------------------------------
    dashboardSidebar(
      br(),
      sidebarMenu(
        menuItem(
          div(id = "frame1", "Frame"),
          bsTooltip(id = "frame1", "Get and format the data frame", placement = "right"),
          
          div(id = "setup1", menuSubItem("Setup Data Frame", tabName = "setup")),
          bsTooltip(id = "setup1", "Build data frame from various source", placement = "right"),
          
          div(id = "summary1", menuSubItem("Frame Summary", tabName = "summary")),
          bsTooltip(id = "summary1", "Show revenue summary and split the frame", placement = "right")
        ),
        
        menuItem(
          div(id = "dataFrame1", "Analysis"),
          bsTooltip(id = "dataFrame1", "Analyze the data frame", placement = "right"),
          
          
          div(id = "cutOffTest1", menuSubItem("CutOff Analysis", tabName = "cutOffTest")),
          bsTooltip(id = "cutOffTest1", "Remove units with small revenue. Note: This is a testing page; selections on this page won\\'t modify the data frame", placement = "right"),
          
          div(id = "allocationTest1", menuSubItem("Allocation Analysis", tabName = "allocationTest")),
          bsTooltip(
            id = "allocationTest1",
            "Allocate sample size of each strata. Note: This is a testing page; selections on this page won\\'t modify the data frame", placement = "right"
          )
        ),
        menuItem(
          div(id = "sample1", "Sample"),
          bsTooltip(id = "sample1", "Draw samples using different sampleing methods", placement = "right"),
          
          div(id = "cutOff1", menuSubItem("CutOff Method", tabName = "cutOff")),
          bsTooltip(id = "cutOff1", "Draw samples with largest units\\' revenue", placement = "right"),
          
          div(id = "PPS1", menuSubItem("PPS Method", tabName = "PPS")),
          bsTooltip(id = "PPS1", "Draw samples with probability proportional to units\\' revenue. ", placement = "right")
          
         # div(id = "SRS1", menuSubItem("SRS Method", tabName = "SRS")),
        #  bsTooltip(id = "SRS1", "Draw simple random sample", placement = "right")
        )
      )
    ),
    
    ## Part 3: Body ------------------------------------
    dashboardBody(
      # CSS that creates the loading bar at the top of the page:
      tags$head(tags$style(type = "text/css", "
                         #loadmessage {
                         position: fixed;
                         top: 50px;
                         left: 0px;
                         width: 100%;
                         padding: 5px 0px 5px 0px;
                         text-align: center;
                         font-weight: bold;
                         font-size: 100%;
                         color: #000000;
                         background-color: #7EC0EE;
                         z-index: 105;
                         }
                         ")),
      conditionalPanel(
        condition = "$('html').hasClass('shiny-busy')",
        tags$div("Loading...", id = "loadmessage")
      ),
      tabItems(
        
        
        ## 1. Set up Data Frame -----------------------------------------
        tabItem(
          tabName = "setup",
          
          h2("Set up Data Frame"),
          fluidRow(
            
            ## 1.1 Input ---------------------------------------------
            column(
              3,
              div(id = "raw1", h4("1. Raw Data Frame")),
              
              bsTooltip(id = "raw1", "Abtract raw data frame from a data source", placement = "left"),
              selectInput("surveyName", "Program Name", c(
                "AESPI-Architecture and Engineering",
                "ASPI-Accounting",
                "CIMERLSPI-Machinery and Equipment Rental",
                "CMSPI-Courriers and Messengers",
                "COSPI-Consulting",
                "CPPI-Computers and Peripherals",
                "CRSPI-Commercial Rents",
                "CSPI-Commercial Software",
                "CUWRPI-Wages",
                "EIPI-Export-Import",
                "EPSPI-Electric Power",
                "FHMCFSPI-Trucking",
                "FIPI-Farm Inputs",
                "FRSPI-Freight Rail",
                "IBSPI -Investment Banking",
                "IPPI/RMPI-Industrial Property/Raw Material",
                "IPSPI-Informatics and Professional Services",
                "MEPI-Machinery Equip",
                "NCAPI-New Condos",
                "NHPI-New Housing",
                "NLSPI-New Lending",
                "PASPI-Passenger Air",
                "PRSPI -Passenger Rail",
                "RBCPI/NRBCPI-Res/Non-Res",
                "RPPI-Residential Property",
                "RRPPI_house-Resale Residential Property",
                "RSPI-Retail",
                "TASPI-Traveller Accom",
                "TSPI-Telecomm",
                "WSPI-Wholesale"
              )),
              selectInput("frameSource", "Data Source", c("Business Register", "Import-Export Registry", "ASML", "Other")),
              
              ## From BR
              conditionalPanel(
                "input.frameSource == 'Business Register'",
                # selectInput("sufDate", "Reference Date (yyyymm)",choices = dateList, selected = dateList[length(dateList)]),
                uiOutput("dateInputUI"),
                bsTooltip(id = "dateInputUI", "Each date is correspond to a tblSUFGeneric table in BRS which is published monthly.", placement = "left"),
                
                
                actionButton("submitBR", "Load"),
                conditionalPanel(
                  "input.submitBR > 0",
                  uiOutput("NAICSInputUI"),
                  
                  radioButtons("BRRegion", "Region", c("10 Provinces", "10 Provinces + 1 Territory", "10 Provinces + 3 Territories")),
                  
                  radioButtons("BRLevel", "Level", c("Enterprise", "Company", "Establishment")),
                  bsTooltip(
                    id = "BRLevel",
                    "Enterprise is an autonomous unit for which a complete set of financial statement is available. Company is the level at which operating profit can be measured. Establishment is the level at which all accounting data require to measure production are available.", placement = "left"
                  )
                )
              ),
              
              ## From Import-Export Registry
              conditionalPanel(
                "input.frameSource == 'Import-Export Registry'",
                selectInput("IEFile", "Frame Version", systemFileList),
                bsTooltip(
                  id = "IEFile",
                  "This list includes all available versions of the IE registry frame. Below is the directory which stores these files", placement = "left"
                ),
                h5(IEPath),
                actionButton("submitIEFile", "Load"),
                conditionalPanel(
                  "input.submitIEFile > 0",
                  uiOutput("HSCodeInputUI"),
                  uiOutput("countryOriginInputUI")
                )
              ),
              
              ## From ASML
              conditionalPanel(
                "input.frameSource == 'ASML'",
                uiOutput("dateASMLInputUI"),
                bsTooltip(id = "dateASMLInputUI", "Each date is correspond to a tblSUFGeneric table in BRS which is published monthly.", placement = "left"),
                
                
                actionButton("submitASML", "Load"),
                conditionalPanel(
                  "input.submitASML > 0",
                  uiOutput("NAPCSInputUI")
                )
              ),
              
              ## From Other
              conditionalPanel(
                "input.frameSource == 'Other'",
                fileInput("otherSource", "Choose CSV File", accept = ".csv")
              ),
              conditionalPanel(
                "input.frameSource == 'Other' || input.submitIEFile > 0 || input.submitBR > 0|| input.submitASML > 0",
                actionButton("submitDataFrame", "Click here to get the raw data frame"),
                bsTooltip(
                  id = "submitDataFrame",
                  "Clink on this button, the raw data frame will be shown at the right panel under Raw Data Frame.", placement = "left"
                )
              ),
              
              br(),
              conditionalPanel(
                "input.submitDataFrame > 0",
                
                div(id = "final1", h4("2. Final Data Frame")),
                bsTooltip(id = "final1", "Formate raw data by assigning indentifier and revenue, and excluding old sample", placement = "left"),
                fluidRow(
                  column(6, uiOutput("keyInputUI")),
                  column(6, uiOutput("revenueInputUI")),
                  bsTooltip(
                    id = "keyInputUI",
                    "This contains all variables in the data frame. Select one as key variable.", placement = "left"
                  ),
                  bsTooltip(
                    id = "revenueInputUI",
                    "This contains all variables in the data frame. Select one as revenue variable.", placement = "right"
                  )
                ),
                radioButtons("oldSample", "Is there any old sample need to be excluded from the data frame?",
                             c("Yes", "No"),
                             inline = TRUE, selected = "No"
                ),
                
                conditionalPanel(
                  "input.oldSample == 'Yes'",
                  fileInput("oldSampleFile", "Choose CSV File", accept = ".csv")
                ),
                actionButton("submitVariable", "Click here to get the final data frame"),
                bsTooltip(
                  id = "submitVariable",
                  "Clink on this button, the final data frame which exclude old sample units will be shown at the right panel under Final Data Frame. Note: User must finish this step before going to the next page. All other pages depend on the first page, but they are independent with each other.", placement = "left"
                )
              )
            ),
            
            ## 1.2 Output --------------------------------------------
            column(
              9,
              tabsetPanel(
                type = "tab",
                tabPanel(
                  "Raw Data Frame",
                  downloadButton("downloadFrame", "Download"),
                  DT::dataTableOutput("data.frame.raw")
                ),
                tabPanel(
                  "Final Data Frame",
                  downloadButton("downloadFinalFrame", "Download"),
                  DT::dataTableOutput("data.frame.final")
                )
              )
            )
          )
        ),
        
        ################## All below pages won't show unless  "input.submitVariable > 0" #####################################
        
        ## 2. Frame Summary --------------------------------------------
        tabItem(
          tabName = "summary",
          h2("Frame Summary"),
          conditionalPanel(
            "input.submitVariable > 0",
            fluidRow(
              
              ## 2.1 Input
              column(
                3,
                uiOutput("revenueDistributionStrataInputUI"),
                bsTooltip(
                  id = "revenueDistributionStrataInputUI",
                  "Choose one as group by variable to view data summary", placement = "left"
                ),
                checkboxInput("splitFrame", "Split the frame"),
                bsTooltip(
                  id = "splitFrame",
                  "If 10 units can give 90% of revenues or there are less than 10 units in the stratum, it will be split into cutoff pool. Otherwise, PPS/SRS", placement = "left"
                ),
                conditionalPanel(
                  "input.splitFrame",
                  radioButtons("showFrame", "Select a frame to keep", c("Cutoff Frame", "PPS/SRS Frame"))
                ),
                actionButton("submitRevenueFrame", "Click here to get the summary")
              ),
              
              ## 2.2 Output --------------------------------------------------
              column(
                9,
                tabsetPanel(
                  type = "tab",
                  tabPanel(
                    "Table 1: Number of units and total revenue",
                    radioButtons("revenueSummaryType", "", c("Table", "Plot"), inline = TRUE),
                    
                    ## two senario
                    conditionalPanel(
                      "input.revenueSummaryType == 'Table'",
                      downloadButton("downloadRevenueSummaryTable", "Download"),
                      DT::dataTableOutput("revenue.summary.table")
                    ),
                    
                    conditionalPanel(
                      "input.revenueSummaryType == 'Plot'",
                      fluidRow(
                        column(3, radioButtons("revenueCountOrValue", "", c( "Total Revenue", "# Units"), inline = TRUE))
                      ),
                      plotOutput("revenue.summary.plot")
                    )
                  ),
                  
                  tabPanel(
                    "Table 2: Revenue Distribution",
                    fluidRow(
                      column(3, radioButtons("revenueDistributionType", "", c("Table", "Plot"), inline = TRUE)),
                      column(3, uiOutput("distributionFilterInputUI")),
                      bsTooltip(
                        id = "distributionFilterInputUI",
                        "This list contains all stratas in selected strata variable. Select one to view data in that strata", placement = "left"
                      )
                    ),
                    
                    ## two senario
                    conditionalPanel(
                      "input.revenueDistributionType == 'Table'",
                      downloadButton("downloadRevenueDistributionTable", "Download"),
                      DT::dataTableOutput("revenue.distribution.table")
                    ),
                    
                    conditionalPanel(
                      "input.revenueDistributionType == 'Plot'",
                      plotlyOutput("revenue.distribution.plot")
                    )
                  ),
                  
                  
                  tabPanel(
                    "Table 3: Data Frame",
                    downloadButton("downloadRevenueDistributionFrame", "Download"),
                    DT::dataTableOutput("revenue.distribution.frame")
                  )
                )
              )
            )
          )
        ),
        
        ## 3. CutOff Analysis -------------------------------------------------
        tabItem(
          tabName = "cutOffTest",
          h2("CutOff Analysis"),
          h4("Remove some units with low revenue"),
          conditionalPanel(
            "input.submitVariable > 0",
            
            ## 3.1 Input
            fluidRow(
              column(
                3,
                radioButtons("cutOffTypeTest", "CutOff from bottom based on ", c("Revenue Share", "Revenue Value")),
                bsTooltip(
                  id = "cutOffTypeTest",
                  "If select \\'Revenue Share\\', units with its revenue precentile less than selected percentage will be removed. If select \\'Revenue Value\\', units with its revenue less than selected value will be removed.", placement = "left"
                ),
                ## Three senario
                conditionalPanel(
                  "input.cutOffTypeTest == 'Revenue Share'",
                  sliderInput("cutOffPercentageTest", "Revenue Share Percentage", min = 0, max = 100, post = "%", value = 10)
                ),
                
                conditionalPanel(
                  "input.cutOffTypeTest == 'Revenue Value'",
                  numericInput("cutOffValueTest", "Revenue Value ($)", 50000)
                ),
                
                
                uiOutput("cutOffStrataTestInputUI"),
               
                actionButton("submitCutOffTest", "Click here to cut the data frame")
              ),
              
              ## 3.2 Output -------------------------------------------------------
              column(9,
                  tabsetPanel(
                    type = "tab",
                    tabPanel(
                      "Table 1: CutOff Summary",
                      radioButtons("showTypeTest", "", c("Table", "Plot"), inline = TRUE),
                      conditionalPanel(
                        "input.showTypeTest == 'Plot'",
                        radioButtons("countOrRevenueTest", "", c( "Total Revenue", "# Units"), inline = TRUE),
                        plotlyOutput("cutOff.test.compare.plot")
                      ),
                      conditionalPanel(
                        "input.showTypeTest == 'Table'",
                        downloadButton("downloadCutOffTestSummary", "Download"),
                        DT::dataTableOutput("cutOff.test.compare")
                      )
                    ),
                    tabPanel(
                      "Table 2: Data Frame after CutOff",
                      downloadButton("downloadCutOffTestFrame", "Download"),
                      
                      
                      
                      DT::dataTableOutput("cutOff.test.frame")
                    )
                  )
                
              )
            )
          )
        ),
        
        
        ## 4. Allocation Analysis -------------------------------------------------------
        tabItem(
          tabName = "allocationTest",
          h2("Allocation Analysis"),
          h4("Allocate sample size in each strata"),
          conditionalPanel(
            "input.submitVariable > 0",
            fluidRow(
              
              ## 4.1 Input
              column(
                3,
                radioButtons("allocationTypeTest", "Allocate based on ", c( "Total Revenue", "# Units")),
                numericInput("sampleSizeTest", "Sample Size", 100),
                uiOutput("allocationStrataTestInputUI"),
                actionButton("submitAllocateTest", "Click here to allocate the data frame")
              ),
              ## 4.2 Output
              column(
                9,
                downloadButton("downloadAllocationTestSummary", "Download"),
                DT::dataTableOutput("allocation.test.summary")
              )
            )
          )
        ),
        
        ## 5. CutOff Method -------------------------------------------------
        tabItem(
          tabName = "cutOff",
          h2("Cutoff"),
          conditionalPanel(
            "input.submitVariable > 0",
            fluidRow(
              
              ## 5.1 Input
              column(
                3,
                h4("1.Allocation"),
                radioButtons("cutOffAllocationType", "Allocate based on", c("Total Revenue", "# Units", "Judgemental", "No Allocation")),
                div(
                  id = "judgemental1",
                  conditionalPanel(
                    "input.cutOffAllocationType == 'Judgemental'",
                    fileInput("cutOff.allocation.judgemental", "Choose CSV File", accept = ".csv")
                  )
                ),
                bsTooltip(
                  id = "judgemental1",
                  "This list includes all csv files in below directory. The selected file must contain at least a variable with same name as selected strata, and a varible named \\'sampleSize\\'.", placement = "left"
                ),
                
                conditionalPanel(
                  "input.cutOffAllocationType != 'Judgemental'",
                  numericInput("cutOffSampleSize", "Sample Size", 100)
                ),
                conditionalPanel(
                  "input.cutOffAllocationType != 'No Allocation' ",
                  uiOutput("cutOffStrataInputUI")
                ),
                conditionalPanel(
                  "input.cutOffAllocationType == '# Units' || input.cutOffAllocationType == 'Total Revenue' ",
                  numericInput("cutOffMinSampleSize", "Minimum Sample Size in Each Strata", 0)
                ),
                h4("2.Attributes Selection"),
                radioButtons("cutOffAttributes",
                             "Need more information (legalname, city, etc.) from BRS?",
                             c("Yes", "No"),
                             inline = TRUE, "No"
                ),
                bsTooltip(
                  id = "cutOffAttributes",
                  "This section is applicable, only if the data frame contains variable named \\'operatingEntityNumber\\'", placement = "left"
                ),
                
                conditionalPanel(
                  "input.cutOffAttributes == 'Yes'",
                 uiOutput("datecutOffInputUI")
                ),
                
                actionButton("submitCutOff", "Click here to get the sample"),
                conditionalPanel(
                  "input.submitCutOff",
                downloadButton("reportCutOff", label = "Generate Report")
                )
              ),
              
              ## 5.2 Output -----------------------------------------------------
              column(
                9,
                tabsetPanel(
                  type = "tab",
                  
                  tabPanel(
                    "Table 1. Sample Summary",
                    radioButtons("showTypeCutOffSummary", "", c("Table", "Plot"), inline = TRUE),
                    conditionalPanel(
                      "input.showTypeCutOffSummary == 'Plot'",
                      plotlyOutput("cutOff.sample.summary.plot")
                    ),
                    
                    conditionalPanel(
                      "input.showTypeCutOffSummary == 'Table'",
                      downloadButton("downloadCutOffSummary", "Download"),
                      DT::dataTableOutput("sample.summary.cutOff")
                    )
                  ),
                  tabPanel(
                    "Table 2. Sample Frame",
                    radioButtons("showTypeCutOff", "", c("Table", "Plot"), inline = TRUE),
                    
                    conditionalPanel(
                      "input.showTypeCutOff == 'Plot'",
                      plotlyOutput("cutOff.sample.plot")
                    ),
                    
                    conditionalPanel(
                      "input.showTypeCutOff == 'Table'",
                      downloadButton("downloadCutOffSample", "Download"),
                      DT::dataTableOutput("sample.cutOff")
                    )
                  )
                )
              )
            )
          )
        ),
        
        ## 6. PPS ----------------------------------------------------------
        tabItem(
          tabName = "PPS",
          h2("PPS"),
          conditionalPanel(
            "input.submitVariable > 0",
            fluidRow(
              
              ## 6.1 Input
              column(
                3,
                
                
                h4("1.CutOff"),
                radioButtons("PPSCutOffType", "Cut Off from bottom based on ", c("Revenue Share", "Revenue Value", "No CutOff")),
                bsTooltip(
                  id = "PPSCutOffType",
                  "If select \\'No CutOff\\', no unit will be removed. If select \\'Revenue Share\\', units with its revenue precentile less than selected percentage will be removed. If select \\'Revenue Value\\', units with its revenue less than selected value will be removed.", placement = "left"
                ),
                ## 3 senario
                conditionalPanel(
                  "input.PPSCutOffType == 'Revenue Share'",
                  sliderInput("PPSCutOffPercentage", "Revenue Share Percentage", min = 0, max = 100, post = "%", value = 10)
                ),
                conditionalPanel(
                  "input.PPSCutOffType == 'Revenue Value'",
                  numericInput("PPSCutOffValue", "Revenue Value ($)", 50000)
                ),
                
                conditionalPanel(
                  "input.PPSCutOffType != 'No CutOff'",
                  uiOutput("PPSCutOffStrataInputUI"),
                  actionButton("submitPPSCutOff", "Click here to cut the data frame")
                ),
                
                
                conditionalPanel(
                  "input.submitPPSCutOff || input.PPSCutOffType == 'No CutOff'",
                  h4("2.Sample Version"),
                  numericInput("randomPPS", "Sample Code", sample(1:100, 1)),
                  h4("3.Allocation"),
                  radioButtons("PPSAllocationType", "Allocation", c("Total Revenue", "# Units", "Judgemental", "No Allocation")),
                  
                  ## senario
                  div(
                    id = "judgemental2",
                    conditionalPanel(
                      "input.PPSAllocationType == 'Judgemental'",
                      fileInput("PPS.allocation.judgemental", "Choose CSV File", accept = ".csv")
                      # selectInput("PPS.allocation.judgemental", "File List", uploadFileList),
                      # h5(paste0(gsub("/", "\\\\", getwd()), "\\UploadData"))
                    )
                  ),
                  bsTooltip(
                    id = "judgemental2",
                    "This list includes all csv files in below directory. The selected file must contain at least a variable with same name as selected strata, and a varible named \\'sampleSize\\'.", placement = "left"
                  ),
                  conditionalPanel(
                    "input.PPSAllocationType != 'Judgemental'",
                    numericInput("PPSSampleSize", "Sample Size", 100)
                  ),
                  
                  conditionalPanel(
                    "input.PPSAllocationType != 'No Allocation'",
                    uiOutput("PPSAllocationStrataInputUI")
                  ),
                  
                  conditionalPanel(
                    "input.PPSAllocationType == '# Units' || input.PPSAllocationType == 'Total Revenue' ",
                    numericInput("PPSMinSampleSize", "Minimum Sample Size in Each Strata", 0)
                  ),
                  
                  h4("4.Variable Selection"),
                  radioButtons("PPSAttributes",
                               "Need more information (legalname, city, etc.) from BRS?",
                               c("Yes", "No"),
                               inline = TRUE, "No"
                  ),
                  bsTooltip(
                    id = "PPSAttributes",
                    "This section is applicable, only if the data frame contains variable named \\'operatingEntityNumber\\'", placement = "left"
                  ),
                  conditionalPanel(
                    "input.PPSAttributes == 'Yes'",
                    # selectInput("sufDatePPS", "Reference Date (yyyymm)", choices = dateList, selected = dateList[length(dateList)])
                    uiOutput("datePPSInputUI")
                  ),
                  actionButton("submitPPS", "Click here to get the sample"),
                  conditionalPanel(
                    "input.submitPPS",
                    downloadButton("reportPPS", label = "Generate Report")
                  )
                )
              ),
              
              ## 6.2 Output -------------------------------------------------------
              column(
                9,
                conditionalPanel(
                  "input.PPSCutOffType != 'No CutOff'",
                  tabsetPanel(
                    type = "tab",
                    
                    tabPanel(
                      "Table 1. CutOff Summary",
                      radioButtons("showTypePPSCutOff", "", c("Table", "Plot"), inline = TRUE),
                      
                      conditionalPanel(
                        "input.showTypePPSCutOff == 'Plot'",
                        radioButtons("countOrRevenuePPS", "", c("Total Revenue", "# Units"), inline = TRUE),
                        plotlyOutput("cutOff.PPS.compare.plot")
                      ),
                      
                      conditionalPanel(
                        "input.showTypePPSCutOff == 'Table'",
                        
                        downloadButton("downloadCutOffPPSSummary", "Download"),
                        
                        DT::dataTableOutput("cutOff.PPS.compare")
                      )
                    ),
                    
                    tabPanel(
                      "Table 2. Cutoff Frame",
                      downloadButton("downloadCutOffPPSFrame", "Download"),
                      DT::dataTableOutput("cutOff.PPS.frame")
                    )
                  )
                ),
                tabsetPanel(
                  type = "tab",
                  tabPanel(
                    "Table 3. Sample Summary",
                    radioButtons("showTypePPSSummary", "", c("Table", "Plot"), inline = TRUE),
                    conditionalPanel(
                      "input.showTypePPSSummary == 'Plot'",
                      plotlyOutput("PPS.sample.summary.plot")
                    ),
                    
                    conditionalPanel(
                      "input.showTypePPSSummary == 'Table'",
                      radioButtons("sampleSummaryType", "", c("Total revenue", "# Units"), inline = TRUE),
                      
                      conditionalPanel(
                        "input.sampleSummaryType == '# Units' ",
                        downloadButton("downloadPPSUnitSummary", "Download"),
                        DT::dataTableOutput("sample.unit.summary.PPS")
                      ),
                      conditionalPanel(
                        "input.sampleSummaryType == 'Total revenue' ",
                        downloadButton("downloadPPSRevenueSummary", "Download"),
                        DT::dataTableOutput("sample.revenue.summary.PPS")
                      )
                    )
                  ),
                  tabPanel(
                    "Table 4. Sample Frame",
                    radioButtons("showTypePPS", "", c("Table", "Plot"), inline = TRUE),
                    
                    conditionalPanel(
                      "input.showTypePPS == 'Plot'",
                      plotlyOutput("PPS.sample.plot")
                    ),
                    
                    conditionalPanel(
                      "input.showTypePPS == 'Table'",
                      downloadButton("downloadPPSSample", "Download"),
                      DT::dataTableOutput("sample.PPS")
                    )
                  )
                )
              )
            )
          )
        )
        
      )
    )
  )
)
