## Program: a_source_ASML.R	
## Author: Yuetong Liu							 
## Date: November, 2019	

## Purpose: 
## This program builds data frame from ASML

## Use: 
## Use the following sourcw statement in the sampling program.                                   
# Version in gitlab: 
# source("https://f3eaipitcap01.statcan.ca/estimation/sampling_shiny_application/raw/master/Program/a_source_ASML.R", echo = TRUE, max.deparse.length = Inf)
# Version in W:
# source(file = "W:/Development/Estimation/Sampling/Dashboard/PreliminaryProgram/Program/a_source_ASML.R")


## The function call is 
# ASMLFrame (ippi.frame, sufdate)
## "ippi.frame" is the ASML data frame
## "sufdate" is the reference month for tblSUFGeneric table in BRS
## This function returns a data frame table derived from ASML and BRS

## Open package
library(haven)
library(dplyr)
library(RODBC)


## function
ASMLFrame <- function(ippi.frame, sufdate){
  
ippi.frame <- ippi.frame%>%
  rename(NAPCS_IPPI=NAPCS_ASM,
         c25_ent_IPPI=c25_est_new)%>%
  mutate(ent_id=I43700)%>%
  filter(c25_ent_IPPI != 0 & !is.na(c25_ent_IPPI))%>%
  select(NAPCS_IPPI, ent_id, c25_ent_IPPI, commCode,I43700,I41700,I43600)

BRS <- odbcDriverConnect(
  "driver={SQL Server};server=BRDSQLSIRPROD;database=SurveyInterfaceRepository; trusted_connection=yes" #server and database(catalog)
)

i <- 1
while (i < nrow(ippi.frame)) {
  if(i+1000 > nrow(ippi.frame)){OEN <- toString(paste("'",ippi.frame$ent_id[i:nrow(ippi.frame)],"'",sep=""))}else{
    j <- i+1000
    OEN <- toString(paste("'",ippi.frame$ent_id[i:j],"'",sep=""))}
  
  sql =sprintf("SELECT OperatingEntityNumber
               from %s
               where OperatingEntityNumber in (%s) and establishmentCode != 0 and BusinessStatusCode = '1'",
               paste("tblSUFGeneric",sufdate, sep=""),OEN)
  
  BR.1 <- sqlQuery(BRS,sql)
  if(i == 1){BR <- BR.1}else{
    BR = rbind(BR,BR.1)
  }
  i = i+1001
}

ippi.frame2<- ippi.frame%>%
  filter(ent_id %in% BR$OperatingEntityNumber)%>%
  arrange(NAPCS_IPPI,c25_ent_IPPI)
  

return(ippi.frame2)

}









