## Program: b_cutoff.R
## Author: Yuetong Liu
## Date: July, 2019

## Purpose:
## This program removes units with small revenue in a data frame.
## There are three functions in this file.
## 1. "CutOffTestRawFrame" cuts the data frame by removing units with small revenue
## 2. "CutOffTestCompare" combines total revenue and # units of cut frame and original frame
## 3. "CutOffTestPlot" plots the stacked histogram of removed and remaining units in the data frame

## Use:
## Use the following source statement in the sampling program.
# Version in gitlab: 
# source("https://f3eaipitcap01.statcan.ca/estimation/sampling_shiny_application/raw/master/Program/b_cutoff.R", echo = TRUE, max.deparse.length = Inf)
# Version in W:
# source(file = "W:/Development/Estimation/Sampling/Dashboard/PreliminaryProgram/Program/b_cutoff.R")

## The function calls are
## 1. CutOffTestRawFrame (dataFrame,cutOffType, strataVar, cutOffPercentage, cutOffValue, revenueVar)
## "dataFrame" is the data frame table.
## "cutOffType" is method used to cut the frame, it is either "Revenue Share" or "Revenue Value".
## "strataVar" is the name of one variable in 'dataFrame'.
## "cutOffPercentage" is percentage of units need to be cut.
## "cutOffValue" is the revenue cutoff point.
## "revenueVar" is the name of one  variable in 'dataFrame'.
## This function returns the cut data frame table.

## 2. CutOffTestCompare (total.revenue, cutOff.frame)
## "dataFrame" is the data frame table.
## "cutOff.frame" is the data table returned in "CutOffTestRawFrame"function.
## "strataVar" is the name of one variable in 'dataFrame'.
## "revenueVar" is the name of one  variable in 'dataFrame'.
## This function returns the 'cutOff.frame' summary table on total revenue and # units before and after cutoff.


## 3. CutOffTestPlot (cutOff.test.compare, showType, surveyName, strataVar){
## "cutOff.test.compare" is the data table returned in "CutOffTestCompare" function.
## "ShowType" is "# Units" or "Total Revenue".
## "surveyName" is the name of the survey.
## "strataVar" is the name of one variable in 'cutOff.test.compare'.
## This function returns the histogram graph "cutoff.plot" converted from 'cutOff.test.compare' table.

## Open package
library(RODBC)
library(dplyr)
library(ggplot2)
library(plotly)

source(file = "./Program/a_revenue_distribution.R")

## 1. CutOffTestRawFrame Function
CutOffTestRawFrame <- function(dataFrame, cutOffType, strataVar, cutOffPercentage, cutOffValue, revenueVar) {

  # Get revenue summary
  tot.revenue <- TotalRevenue(dataFrame, strataVar, revenueVar) # in a_revenue_distribution.R
  
  # value/share
  if (cutOffType == "Revenue Value") {
    cutOff.frame <- merge(dataFrame, tot.revenue) %>%
      arrange(!!(as.name(strataVar)), !!(as.name(revenueVar))) %>%
      filter(!!(as.name(revenueVar)) >= cutOffValue)
  } else {
    rev.share <- RevenueShare(dataFrame,tot.revenue,strataVar,revenueVar) # in a_revenue_distribution.R
    
    cutOff.frame <- rev.share %>%
      filter(cumshare > (cutOffPercentage / 100))
  }
  return(cutOff.frame)
}

## 2. CutOffTestCompare Function
CutOffTestCompare <- function(dataFrame, cutOff.frame, strataVar, revenueVar) {
  
  # total sum
  tot.summary <- dataFrame %>%
    group_by(!!(as.name(strataVar))) %>%
    summarise(
      strataCount = n(),
      strataRevenue = as.numeric(sum(!!(as.name(revenueVar)), na.rm = TRUE))
    )
  
  # cut sum
  cut.summary <- cutOff.frame %>%
    group_by(!!(as.name(strataVar))) %>%
    summarise(
      strataCountAfterCutOff = n(),
      strataRevenueAfterCutOff = as.numeric(sum(!!(as.name(revenueVar)), na.rm = TRUE))
    )
  
  
  cutOff.test.compare <- merge(tot.summary, cut.summary, by=strataVar, all.x = TRUE ) %>%
    rbind(list(
      as.factor("Total"),
      sum(.$strataCount),
      sum(.$strataRevenue),
      sum(.$strataCountAfterCutOff),
      sum(.$strataRevenueAfterCutOff)
    ))
  
  cutOff.test.compare[is.na(cutOff.test.compare)] <- 0
  
  return(cutOff.test.compare)
}


## 3. CutOffTestPlot Function
CutOffTestPlot <- function(cutOff.test.compare, showType, surveyName, strataVar) {
  removed <- cutOff.test.compare %>%
    mutate(
      count = strataCount - strataCountAfterCutOff,
      revenue = strataRevenue - strataRevenueAfterCutOff,
      countPercent = round((count / strataCount) * 100, 2),
      revenuePercent = round((revenue / strataRevenue) * 100, 2),
      type = "removed"
    ) %>%
    select(-c(strataCount:strataRevenueAfterCutOff)) %>%
    .[-nrow(cutOff.test.compare), ]

  remained <- cutOff.test.compare %>%
    mutate(
      count = strataCountAfterCutOff,
      revenue = strataRevenueAfterCutOff,
      countPercent = round((count / strataCount) * 100, 2),
      revenuePercent = round((revenue / strataRevenue) * 100, 2),
      type = "remained"
    ) %>%
    select(-c(strataCount:strataRevenueAfterCutOff)) %>%
    .[-nrow(cutOff.test.compare), ]

  frame.for.chart <- rbind(removed, remained)

  cutoff.plot <- ggplot(data = frame.for.chart, aes(
    x = !!(as.name(strataVar)),
    y = if (showType == "# Units") {
      !!(as.name("count"))
    } else {
      !!(as.name("revenue"))
    },
    text = sprintf(
      "%s: %s 
                                                        %s: %s 
                                                        %s: %s 
                                                        %s: %s",
      "Strata", !!(as.name(strataVar)),
      "Type", type,
      "# Units", paste0(count, " (", countPercent, "%)"),
      "Revenue", paste0(revenue, " (", revenuePercent, "%)")
    )
  )) +
    geom_col(aes(fill = type)) +
    theme(
      axis.text.x = element_text(angle = 45, hjust = 1),
      plot.title = element_text(hjust = 0.5),
      legend.title = element_blank()
    ) +
    xlab(strataVar) +
    ylab(showType) +
    ggtitle(paste(surveyName, "CutOff Summary on", showType))

  return(ggplotly(cutoff.plot, tooltip = c("text")))
}
