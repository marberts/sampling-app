%sysmstoreclear;
%global _gsampath_;
%let _gsampath_ = T:\Data Exchange\Estimation\Estimation-BSMD\RShiny\win64-sas94m3-nosecure;
options mstored sasmstore=gsam;
libname gsamcat "&_GSAMPATH_";
libname NR "T:\Data Exchange\Estimation\Estimation-BSMD\RShiny\GSAM";

options
   spool
   locale=en_ca
   linesize=120
   pagesize=max
   noMPRINT noMLOGIC noSYMBOLGEN
   mstored sasmstore=gsamcat
;
proc import datafile = "T:\Data Exchange\Estimation\Estimation-BSMD\RShiny\GSAM\stratification_in.csv"
out = population 
dbms=csv replace;
run;

data population; set population; cellid  = region; unitid = operatingentitynumber; run; 


/* Create the GlobalParameters data set containing the stratification specifications. */
data globalparameters;
/* First define the stratification variables. */
length stratificationmethod $ 16    
       targetvar $ 16   
       substratnum 8
       maxiter 8
     	 stopiter 8     
       maxiter 8        
       cv 8;
/* Then assign values to the stratification variables. */
stratificationmethod = "LH"; /* stratification method */
targetvar = "netarev"; /* target variable */
substratnum = 3; /* number of strata */
maxiter = 1E5;   /* maximum number of iterations of the L-H algorithm */
stopiter = .;    /* terminate the iterative process if the maximum of successive 
                    differences of calculated bounds is less than this amount.                  
                    Default=1E-10.*/
cv = .1;        /* coefficient of variation */
output;
run;

/* Run the %gsam_stratification() macro.  The 2 outputs, outDefinition and outFrame, will stored in the default SAS WORK folder. */
%gsam_stratification(
/* Assign macro parameters. */
globalparameters = globalparameters, /* user-supplied stratification parameters */
population = population,             /* user-supplied population */
outdefinition = NR.stratification_def,          /* output containing cell stratification info */
outframe = NR.stratification_out              /* output containing stratified population */
);

